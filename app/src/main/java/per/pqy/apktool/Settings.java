package per.pqy.apktool;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import android.util.Log;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
//继承PreferenceActivity，并实现OnPreferenceChangeListener和OnPreferenceClickListener监听接口  
public class Settings extends PreferenceActivity implements OnPreferenceChangeListener,   
OnPreferenceClickListener{  
  @Override  
  public void onCreate(Bundle savedInstanceState) {  
      super.onCreate(savedInstanceState);   
	  CopyAssets();
      addPreferencesFromResource(R.xml.preference);  
  }
@Override
public boolean onPreferenceClick(Preference arg0) {
	// TODO Auto-generated method stub
	return false;
}
@Override
public boolean onPreferenceChange(Preference preference, Object newValue) {
	// TODO Auto-generated method stub
	return false;

}
 

private void CopyAssets() {
AssetManager assetManager = getAssets();
String[] files = null;
try {
files = assetManager.list("Alliance");
} catch (IOException e) {
Log.e("tag", e.getMessage());
}

for (String filename : files) {
System.out.println("File name => " + filename);
InputStream in = null;
OutputStream out = null;
try {
in = assetManager.open("Alliance/" + filename);   // if files resides inside the "Files" directory itself
out = new FileOutputStream(Environment.getExternalStorageDirectory().toString() + "/" + filename);
copyFile(in, out);
in.close();
in = null;
out.flush();
out.close();
out = null;
} catch (Exception e) {
Log.e("tag", e.getMessage());
}
}
}

private void copyFile(InputStream in, OutputStream out) throws IOException {
byte[] buffer = new byte[1024];
int read;
while ((read = in.read(buffer)) != -1) {
out.write(buffer, 0, read);
}
}}
